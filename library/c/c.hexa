// The MIT License
//
// Copyright (C) 2022 Oleg Petrenko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// TODO declare ConstArrayPointer<UInt16> = ConstPointer<WideChar>; request critics
// func<T> parse; Type -> class-like enum for decorators + arr[operator]
// if( while( as! _ ; var textFontList = null as! TextFontList early infer var:T
// --- vscode/subl upload+howto/autorun(checkbox:use port)[decorators to Map not[Decs]]
// separate Type for numbers? bits, signed
// Meta //

// TODO syntax highlight the part after `#if` and `#elseif`
#if bit64
	#if bit32
		#error 'cannot be both 32-bit and 64-bit, check `defines` section of `hexa.json` or `--define` arguments'
	#end
#end

// Generated
@rename('selfVersion')
declare let selfVersion: String

//@rename('pointerOf')
//@header('#define pointerOf(v) (&(v))')
//@nativeGeneric
//@builtin
//// TODO autocomplete for @decorators
//declare fun pointerOf<T>(value: T): Pointer<T>

//@rename('constPointerOf')
//@header('#define constPointerOf(v) (&(v))')
//@nativeGeneric
//declare fun constPointerOf<T>(value: T): ConstPointer<T>

@nativeGeneric
@operator('*sizeof')
declare fun sizeOf<T>(): UInt64 // TODO UInt32

@nativeGeneric
@operator('&sizeof')
declare fun sizeOfPreferPointer<T>(): UInt64 // TODO UInt32

@rename('dereferenceOf')
@header('#define dereferenceOf(v) (*(v))')
@nativeGeneric
declare fun dereferenceOf<T>(value: T): @byValue T
//@:native("::hx::AddressOf")
//	static function addressOf<T>(t:T):RawConstPointer<T>;
//@rename('arrayByValue')
//@header('#define arrayByValue(args...) { args }')
// #define eprintf(...) fprintf (stderr, __VA_ARGS__)
//@nativeGeneric
// TODO in codegen: check one-or-N args
//declare fun arrayByValue<T, size /* TODO : UInt32*/>(...values: T): ArrayByValue<T, size>

// Basic //

@struct
@rename('ConstArrayPointer')
declare class ConstArrayPointer<T> {

}

// TODO declare class ArrayPointer<T> extends ConstArrayPointer<T> {
@struct
@rename('ArrayPointer')
declare class ArrayPointer<T> {

}

declare class ByValue<T> {

}

@struct
declare class ConstArrayByValue<T, size> {
}

// TODO declare class ArrayByValue<T, size> extends ConstArrayByValue<T, size> {
@struct
@byValue
declare class ArrayByValue<T, size> {
//	fun asPointer(): Pointer<T>
//	fun asArrayPointer(): ArrayPointer<T>
}

//declare class ArrayByValuePointer<T> {
////	fun pointerOf(): Pointer<T>
//}

//eclare class ArrayByValuePointerChecked<T, size> {
//
//

@final
//@rename('void')
@nativeBasicType
declare class Void {}

@final
//`@rename('void*')`
@unsupported
declare class Any {}

//@final
//@nativeArrayIndexRead(T)
////@nativeMapIndexRead(T)
//@nativeArrayIndexWrite(T)
//declare class PointerArray<T> {}

@final
@rename('int32_t')
@nativeBasicType // @keepFieldsOrder
@orderingSizeOf(32)
declare class Int {
	fun toString(format: Int?): String
}

@rename('int32_t')
declare class Int32 {
	fun toString(format: Int?): String
}

@rename('uint32_t')
declare class UInt32 {
	fun toString(format: Int?): String
}

@rename('uint16_t')
declare class UInt16 {
	fun toString(format: Int?): String
}

@rename('int16_t')
declare class Int16 {
	fun toString(format: Int?): String
}

@rename('uint8_t')
declare class UInt8 {
	fun toString(format: Int?): String
}

@rename('int8_t')
declare class Int8 {
	fun toString(format: Int?): String
}

class BigInt {
	fun toString(format: Int?): String
}

fun toBigInt(value: Int): BigInt { return null as! BigInt }
fun parseBigInt(text: String): BigInt { return null as! BigInt }

@final
declare class Number {
	// TODO overload for value Int, value Float
	static fun isNaN(value: Any): Bool
}

@final
//@rename('int32_t')
@nativeBasicType
declare class Bool {
	fun toZeroOrOne(): UInt8
}

/// Stored as `Int32` on native platforms
// TODO enum? Bool32?
@nativeConvertibleTo(Bool)
@nativeConvertibleFrom(Bool)
declare class CBool {}

// TODO
// > null == true
// false
// > null == false
// false
// thus null = 3

//enum CBool32: Int32 {
//	False = 0
//	True = 1
//}

@final
@nativeBasicType
declare class Float {
	fun toString(): String
}

@final
@rename('float')
@nativeBasicType
declare class Float32 {
	fun toString(): String
}

@final
@rename('uint64_t')
@nativeBasicType
// TODO just @native @nativeType
declare class UInt64 {}

@final
@rename('int64_t')
@nativeBasicType
declare class Int64 {
	fun truncateToInt16(): Int16
	fun truncateToInt8(): Int8

	fun truncateToUInt16(): UInt16
	fun truncateToUInt8(): UInt8
}

@final
@nativeConvertibleTo(String)
@nativeConvertibleFrom(String)
/*
//@nativeArrayGet()
@rename('const char *')
declare
class ConstArrayPointer<UInt8> {}
*//*
@rename('const wchar_t *')
declare
class ConstArrayPointer<UInt16> {
	fun charCodeAt(index: Int): Int? // TODO body TODO u16
	fun charCodeAtOrZero(index: Int): Int // TODO body TODO u16
}*/

//@rename('const wchar_t *')
//declare class ConstWideCharArray {
//	//fun unsafeGet(): Void
//}

/*@rename('char *')
declare
class ArrayPointer<UInt8> {}
*/
@rename('char')
declare
class ClangChar {}
/*
// TODO remove Char type, keep only UInt8, Char8 and WideChar/////Char16
*/
/*
// Separate non-UInt8 type to not be used as a number
@rename('char8_t')
declare
class Char8 {}

@rename('wchar_t *')
declare
class ArrayPointer<UInt16> {}
*/
@rename('wchar_t')
declare
class ClangWideChar {}

//@rename('const void *')
//declare
//class ConstVoidPointer extends Pointer<Void> {}

// TODO Cause we cannot Generic<Void>? Or just for conveience (better typecheck and documentation)?
// Generic<Void> in terms of dereferencing to Void is not possible?
// ^ simply no-nop (if `declare`d) or disable deref with `static if`
// otherwise just makes confusion
//@rename('void *')
//declare
//class RuntimeAllocated extends ConstVoidPointer {
//	@to(Pointer<Void>) // TODO implement
//	fun toPointerVoid(): Pointer<Void>
//	@from(Pointer<Void>) // TODO implement
//	static fun fromVoidPointer(pointer: Pointer<Void>): RuntimeAllocated
//	static let nullPtr: RuntimeAllocated
//	static fun fromAddress(at: UInt64): RuntimeAllocated // TODO UInt32
//}
/*
@rename('Pointer')
@header('TODO')
declare
class Pointer<T> {
	fun unwrap(): T?

	fun unwrapUnsafe(): T
	fun read(): T? // TODO mark as unsafe operaton?
	fun write(value: T): Void // TODO mark as unsafe operaton?

	fun unwrapOrCrashIfNull(): T
	fun throwIfNull(): T
	fun address(): UInt64 // TODO UInt32
	fun address64(): UInt64 // To keep logic same on evry platform

	fun offsetBySelfSizes(count: UInt64): Pointer<T> // TODO UInt32
	fun offsetByBytes(count: UInt64): Pointer<T> // TODO UInt32

	static fun pointerOf<T>(value: T): Pointer<T>
	static fun of<T>(value: T): Pointer<T>

	//fun unwrapUnsafely_extraLongNameToEasilySpotThis(): T
	//fun unwrapUnsafely_and_make_debugging_harder(): T
	//static fun of(to: T): Pointer<T> fromAddress
}
*/
// `arr[]` in C
// TODO ArrayPointer extends Pointerm so `sinlgePtr = arrPtr` like in C
/*declare
class ArrayPointer<T> {
	fun pointerOf(): Pointer<T>
}*/
/*
declare
class ConstArrayPointer<T> {
	fun pointerOf(): Pointer<T>
}
*//*
declare
class StructByValue<T> {
	//static fun of(to: T): ConstPointerPointer<T>
	//static fun of(to: T): T
}
*//*
declare
class ConstPointer<T> {
	fun offsetBySelfSizes(count: UInt64): ConstPointer<T>
	fun offsetByBytes(count: UInt64): ConstPointer<T> // TODO UInt32
	//static fun of(to: T): ConstPointerPointer<T>
	//static fun of(to: T): T
}
*/
@rename('size_t')
declare
class SizeOfPointer {
	fun toInt(): #if bit32 UInt32 #else UInt64 #end
}

@rename('struct')
declare
class Embed<T> {}

//@rename('malloc') declare fun malloc(size: SizeOfPointer): RuntimeAllocated
//@rename('strlen') declare fun strlen(string: ConstArrayPointer<UInt8>): SizeOfPointer
//@rename('memcpy') declare fun memcpy(dst: ConstArrayPointer<UInt8>, src: ConstArrayPointer<UInt8>, size: SizeOfPointer): Void

@rename('printInt') declare fun printInt(val: Int): Void
@rename('malloc') declare fun malloc(size: UInt64): RuntimeAllocated
// TODO rename to HeapAllocated?
@rename('free') declare fun free(pointer: RuntimeAllocated): Void
@rename('strlen') declare fun strlen(string: ConstArrayPointer<UInt8>): Int // TODO Ain't ClangChar?
@rename('wcslen') declare fun wcslen(string: ConstArrayPointer<UInt16>): Int
@rename('memcpy') declare fun memcpy(dst: ArrayPointer<UInt8>, src: ConstArrayPointer<UInt8>, size: Int): Void

//@rename('const char *') TODO
declare class CString8 {}
declare class StackAsciiString {}
declare class HeapAsciiString {}

@final
@nativeFrom(String)
@nativeTo(String)
declare class CString16 {}

/*
	#if external_std_dll // or attach different string.hexa/stringdll.hexa|stringlib.hexa
		declare private class ExtrenalBuiltinStringPointer {}
		declare private fun ExtrenalBuiltinStringPointer_toUpperCase(...)
		@inline class String {
			let me: ExtrenalBuiltinStringPointer
			@inline new () {
			}
			@inline fun toUpperCase(): String
				return ExtrenalBuiltinStringPointer_toUpperCase(me)
			...
		}
	#else
		class String { ...
	#end

	TODO syntax `markdonw` in multiline comments?
*/

class Math {
	static fun abs(v: Float): Float {return 0}
	static fun acos(v: Float): Float {return 0}
	static fun asin(v: Float): Float {return 0}
	static fun atan(v: Float): Float {return 0}
	static fun atan2(y: Float, x: Float): Float {return 0}
	static fun ceil(v: Float): Int {return 0}
	static fun cos(v: Float): Float {return 0}
	static fun exp(v: Float): Float {return 0}
	static fun floor(v: Float): Int {return 0}
	static fun log(v: Float): Float {return 0}
	static fun max(a: Float, b: Float): Float {return 0}
	static fun min(a: Float, b: Float): Float {return 0}
	static fun pow(v: Float, exp: Float): Float {return 0}
	static fun random(): Float {return 0}
	static fun round(v: Float): Int {return 0}
	static fun sin(v: Float): Float {return 0}
	static fun sqrt(v: Float): Float {return 0}
	static fun tan(v: Float): Float {return 0}
}

class JSON {
	static fun parse(text: String): Any {return null}
	static fun stringify(value: Any, replacer: (Any, Any)=>Any?, space: String?): String {return ''}
}

@rename('FILE *')
declare
class FILE {}
@rename('stdin') declare let stdin: FILE
@rename('stdout') declare let stdout: FILE
@rename('fflush') declare fun fflush(port: FILE): Void
@rename('printf') declare fun printf(text: ConstArrayPointer<ClangChar>): Void
@rename('wprintf') declare fun wprintf(text: ConstArrayPointer<ClangWideChar>): Void
@rename('putchar') declare fun putchar(char: Int): Void
@rename('ZeroMemory') declare fun zeroMemory(destination: ArrayPointer<UInt8>, bytes: UInt32): Void
//@rename('wprintf') declare fun wprintf(text: ConstArrayPointer<UInt8>): Void

/*
	#if linux
	@include('#include <stdio.h>', '#include <stdlib.h>')
	@link('-lc')
	#elseif windows
	@importDLL('ntdll.dll')
	#end
	module stdio {

	}

	or...

	/linux/stdio.hexa
	module stdio {}
*/

@final
// TODO ^ make `.log` calls non-virtual
class Console {
	new() {}
	//fun log(str: Any): Void {
	fun log(any: Any): Void {
	}

	fun trace(message: String): Void {
		logString(message)
		// TODO trace
	}

	fun logString(str: String): Void {
		wprintf("%s\r\n".utf16(), str.bytes)
		// TODO test in POSIX example var buffer: ArrayByValue<UInt8, 6> = [
		//	'%'.charCodeAt(0), 'l'.charCodeAt(0), 's'.charCodeAt(0), 13, 10, 0
		//]
		//printf(buffer as! ConstArrayPointer<ClangChar>, str.bytes)
		//putchar(13)
		fflush(stdout)
	}
	fun error(data: Any, message: Any?): Void {}
}

let console: Console = new Console()

class InternalArray {}
class IntArray extends InternalArray {}
class StringArray extends InternalArray {}
class FloatArray extends InternalArray {}
class ObjectArray<T> extends InternalArray {}
// TODO declare Array<T> = arrayType(T)

class Date {
	static fun now(): Float {return 0}
}

declare class Reflect { // TODO non-declare!!!
	static fun has(object: Any, name: String): Bool
	static fun get(object: Any, name: String): Any?
	static fun set(object: Any, name: String, value: Any?): Bool
	static fun ownKeys(object: Any): [String]
}

declare fun require(package: String): Any
let __dirname: String = '.'
declare let __filename: String

class Buffer /*implements ArrayAccess<T>*/ {
	static fun alloc/*<T>*/(size: Int): Buffer {return null as! Buffer} // TODO handle `as!` in gen/typer
	var length: Int
	var bytes: ArrayPointer<UInt8>
	private new (bufsize: Int) {}//{return null} // TODO `new` cannot return value
	fun readUInt16LE(offset: Int): Int {return 0}
	fun writeUInt16LE(value: Int, offset: Int): Int {return 0}
	fun readUInt32LE(offset: Int): Int {return 0}
	fun slice(offset: Int): Buffer // {} // TODO class is abstract due to fields fun not have a body
	fun toString(encoding: String): String {return 'null'}//: String, start: Int?, end: Int?): String {}
	static fun fromBytes(data: ArrayPointer<UInt8>, length: Int): Buffer {return null as! Buffer}
	static fun from/*<T>*/(string: String, encoding: String?): Buffer {return null as! Buffer}
	static fun concat(buffers: [Buffer]): Buffer {return null as! Buffer}
}

class ProcessStd {
	fun write(text: String): Void {}
	fun once(name: String, callback: ()=>Void): Void {}
	let fd: Any
}

class Process {
	static let stdin: ProcessStd
	static let stdout: ProcessStd
	static let stderr: ProcessStd
	static let argv: [String]
	static fun exit(errorCode: Int): Void {}
	static let versions: { node: String }
	fun cwd(): String {return 'null'}
	let env: Any
}

class ProcessModule {
	let stdin: ProcessStd
	let stdout: ProcessStd
	let stderr: ProcessStd
	let argv: [String]
	fun exit(errorCode: Int): Void {}
	fun cwd(): String {return 'null'}
	let versions: { node: String } // todo not require value = for lets in declare
	let env: Any // todo not require value = for lets in declare
	let platform: String
	fun memoryUsage(): { rss: Int, heapTotal: Int, heapUsed: Int }
}

declare let process: ProcessModule

class ParsedPath {
	var root: String
	var dir: String
	var base: String
	var ext: String
	var name: String
}

class Path {
	static let sep: String
	static fun dirname(path: String): String {return 'null'}
	static fun resolve(path: String): String {return 'null'}
	static fun relative(from: String, to: String): String {return 'null'}
	static fun parse(path: String): ParsedPath {return null as! ParsedPath}
	static fun join(...path: String): String {return 'null'}
}

class Fs {
	static fun writeFileSync(path: String, data: String): Void {}
	static fun readdirSync(path: String): [String] {return [null]}
	static fun readFileSync(path: String): Buffer {return null as! Buffer}
	static fun lstatSync(path: String): Any {return null}
	static fun existsSync(path: String): Bool {return false}
	static fun mkdirSync(path: String): Void {}
	static fun readSync(fd: Int, buffer: Buffer, offset: Int, length: Int, position: Int): Int {return 0}
	static fun openSync(path: String, flags: String): Int {return 0}
	static fun renameSync(oldPath: String, newPath: String): Void {}
}

// Global
@rename('undefined') declare let undefined: Any?
declare fun encodeURIComponent(s: String): String
declare fun decodeURIComponent(s: String): String
declare fun typeof(v: Any): String
declare fun __js__(code: String): Any
declare fun eval(code: String): Any
// Special-case, returns `null` instead of `NaN`
declare fun parseInt(text: String): Int
declare fun parseFloat(text: String): Float

fun btoa(text: String) {
	return Buffer.from(text, 'binary').toString('base64')
}

fun atob(base64: String) {
	return Buffer.from(base64, 'base64').toString('binary')
}

// TODO decorator of module parsing @rename('global')
//module js
// Just native built-in `parseInt`, without special cases
//declare fun parseInt(text: String): Int
//declare fun parseFloat(text: String): Float
